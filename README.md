# Top Trumps
#### Completed by Tabrez Akhtar

### Description
This models the game of Top Trumps.  It will create players, shuffle the deck, return the highest card attribute when a card is selected.

### How to run

The test is done with JavaScript.  It will require [Node](https://nodejs.org/en/download/package-manager/) to be installed.

To run the app:

`npm install`

`node index`

Run unit tests:  
`npm test`


### Improvements
* Only 1 round is played at the moment.
* When a round is won, the cards need to be removed from the losing players and added to the winner.
* There needs to be a game loop to keep track of current player and round.
* If there is a draw, the selected cards should be stored and added to the winners cards in the next round.
* Needs some more unit tests