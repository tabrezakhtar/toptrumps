const _ = require('lodash');

function createPlayer(players, name) {
  return {id: players.length, name}
}

function getOpponents(players, id) {
  return players.filter(p => p.id !== id);
}

function dealCards(shuffledCards, players) {
  const cardLength = Math.floor(shuffledCards.length / players.length);
  return players.map((player, index) => ({
    ...player,
    cards: shuffledCards.slice(index * cardLength, ((index + 1) * cardLength))
  }));
}

function getRandomAttribute(attribute) {
  const attrArray = Object.keys(attribute).map(key => ({ key, value: attribute[key] }));
  return attrArray[_.random(0, attrArray.length - 1)];
}

function getRandomCards(players) {
  return players.map(p => _.sample(p.cards));
}

function shuffleCards(cards) {
  return _.shuffle(cards);
}

function removeCard(cards, card) {
  return cards.filter(c => c.name !== card.name);
}

function getWinningCard(selectedAttribute, activeCard, opponentCards) {
  const foundWinningCard = opponentCards.reduce((previous, current) =>
    previous.attributes[selectedAttribute.key] > current.attributes[selectedAttribute.key] ? previous : current);
  return foundWinningCard.attributes[selectedAttribute.key] > activeCard.attributes[selectedAttribute.key] ? foundWinningCard : activeCard;
}

module.exports = {createPlayer, dealCards, getOpponents, getRandomAttribute, getRandomCards, shuffleCards, removeCard, getWinningCard}