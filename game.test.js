const playerUtils = require('./playerUtils');

const players = [];

beforeEach(() => {  
  players.push(playerUtils.createPlayer(players, 'player1'));
  players.push(playerUtils.createPlayer(players, 'player2'));
  players.push(playerUtils.createPlayer(players, 'player3'));
});

describe('in a 3 player game', () => {
  describe('when the current player has a winning attribute', () => {
    it('should return the current active card', () => {
      const activeCard = { name: 'Harry Potter', attributes:  { magic: 95, cunning: 40, courage: 80, wisdom: 100, temper: 8 } };
      const selectedAttribute = { key: 'magic', value: 95 }

      const opponentsCards = [
        { name: 'Draco Malfoy', attributes: { magic: 60, cunning: 35, courage: 30, wisdom: 28, temper: 21 } },
        { name: 'Ron Weasley',  attributes: { magic: 80, cunning: 25, courage: 70, wisdom: 60, temper: 10 } }
      ];

      const winningCard = playerUtils.getWinningCard(selectedAttribute, activeCard, opponentsCards);
      expect(winningCard).toEqual(activeCard);
    });
  });

  describe('when the current player has a losing attribute', () => {
    it('should return the current winning card', () => {
      const activeCard = { name: 'Harry Potter', attributes:  { magic: 95, cunning: 40, courage: 80, wisdom: 100, temper: 8 } };
      const selectedAttribute = { key: 'cunning', value: 40 }

      const opponentsCards = [
        { name: 'Draco Malfoy', attributes: { magic: 60, cunning: 50, courage: 30, wisdom: 28, temper: 21 } },
        { name: 'Ron Weasley',  attributes: { magic: 80, cunning: 25, courage: 70, wisdom: 60, temper: 10 } }
      ];

      const winningCard = playerUtils.getWinningCard(selectedAttribute, activeCard, opponentsCards);
      expect(winningCard).toEqual({ name: 'Draco Malfoy', attributes: { magic: 60, cunning: 50, courage: 30, wisdom: 28, temper: 21 } });
    });
  });
});