const cards = require('./potter.json');
const playerUtils = require('./playerUtils');

const players = [];
players.push(playerUtils.createPlayer(players, 'player1'));
players.push(playerUtils.createPlayer(players, 'player2'));
players.push(playerUtils.createPlayer(players, 'player3'));

const shuffledCards = playerUtils.shuffleCards(cards);
const playersWithCards = playerUtils.dealCards(shuffledCards, players);
const currentPlayer = playersWithCards[0];
const activeCard = currentPlayer.cards[0];
const opponents = playerUtils.getOpponents(playersWithCards, currentPlayer.id);

const state = {
  players: playersWithCards,
  currentPlayer,
  activeCard,
  selectedAttribute: playerUtils.getRandomAttribute(activeCard.attributes),
  opponents,
  opponentsCards: playerUtils.getRandomCards(opponents)
}

const winningCard = playerUtils.getWinningCard(state.selectedAttribute, state.activeCard, state.opponentsCards);

console.log('player 1 selected card', state.activeCard);
console.log('player 1 selected card attribute', state.selectedAttribute);
console.log('opponent cards', state.opponentsCards);
console.log('winning card', JSON.stringify(winningCard));